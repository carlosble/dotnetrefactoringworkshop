using NUnit.Framework;
using valueAndReference;

namespace Tests
{


    public class YouShouldUnderstandValuesAndReferences
    {
        [Test]
        public void because_its_fundamental()
        {
            SomeType instance = new SomeType();

            string arg1 = "bar";
            instance.firstMethod(arg1);
            // arg1.Should().Be("???");

            SomeType other = new SomeType();
            other.numbers[0] = 100;
            instance.secondMethod(other);
            // other.numbers[0].Should().Be(-1000);
            // other.someField.Should().Be("???");

            SomeType another = new SomeType();
            another.numbers[0] = 200;
            instance.thirdMethod(another);
            // another.numbers[0].Should().Be(-1000);
            // another.someField.Should().Be("???");
            // another.child).isNull();

            SomeType yetAnother = new SomeType();
            yetAnother.numbers[0] = 200;
            instance.fourthMethod(yetAnother.numbers);
            // yetAnother.numbers[0].Should().Be(-1000);
        }
    }
}