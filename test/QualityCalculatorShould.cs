using FluentAssertions;
using NUnit.Framework;
using replacePrimitiveConstantsWithType;

namespace Tests
{



    public class QualityCalculatorShould
    {

        [Test]
        public void calculate_score()
        {
            QualityCalculator calculator = new QualityCalculator();
            calculator.rateFacilities(Quality.Excellent);
            calculator.rateLocation(Quality.Excellent);

            calculator.getScore().Should().Be(12);
        }
    }
}
