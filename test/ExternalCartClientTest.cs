using implementationChange;
using NUnit.Framework;

namespace Tests
{
    public class ExternalCartClientTest
    {
        [Test]
        public void format_total_price_message()
        {
            ExternalCartClient client = new ExternalCartClient();

            Assert.AreEqual("Total price is 50 euro", client.formattedTotalPrice(50));
        }
    }
}