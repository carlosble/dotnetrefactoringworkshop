using implementationChange;
using NUnit.Framework;

namespace Tests
{

    public class ShoppingCartShould
    {
        [Test]
        public void count_number_of_products()
        {
            ShoppingCart shoppingCart = new ShoppingCart();
            shoppingCart.Add(10);

            Assert.AreEqual(1, shoppingCart.NumberOfProducts());
        }

        [Test]
        public void calculate_total_price()
        {
            ShoppingCart shoppingCart = new ShoppingCart();
            shoppingCart.Add(10);

            Assert.AreEqual(10, shoppingCart.CalculateTotalPrice());
        }

        [Test]
        public void know_when_is_discount_applicable()
        {
            ShoppingCart shoppingCart = new ShoppingCart();
            shoppingCart.Add(100);

            Assert.True(shoppingCart.HasDiscount());
        }

        [Test]
        public void know_when_is_not_possible_to_apply_discount()
        {
            ShoppingCart shoppingCart = new ShoppingCart();
            shoppingCart.Add(99);

            Assert.False(shoppingCart.HasDiscount());
        }

    }
}