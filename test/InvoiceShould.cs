using FluentAssertions;
using moveResponsibility;
using NUnit.Framework;

namespace Tests
{
    public class InvoiceShould
    {

        [Test]
        public void calculate_net_amount()
        {
            InvoiceService invoiceService = new InvoiceService();

            invoiceService.CalculateNetAmount("100", "10").Should().Be("90");

        }
    }
}
