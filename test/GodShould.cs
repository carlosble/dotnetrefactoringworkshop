using moveResponsibility;
using NUnit.Framework;
using FluentAssertions;

namespace Tests
{
    public class GodShould
    {
        [Test]
        public void add_numbers()
        {
            God god = new God();
            god.Add(2, 2).Should().Be(4);
        }

        [Test]
        public void substract_numbers()
        {
            God god = new God();
            god.Substract(2, 2).Should().Be(0);
        }
    }
}
