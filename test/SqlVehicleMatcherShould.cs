using FluentAssertions;
using NUnit.Framework;
using replaceType;

namespace Tests
{

    public class SqlVehicleMatcherShould
    {
        [Test]
        public void find_vehicles()
        {
            SqlVehicleMatcher matcher = new SqlVehicleMatcher();
            OldVehicleQuery query = new OldVehicleQuery();
            query.brandDescription = "VW Polo";

            matcher.CountVehicles(query).Should().Be(1);
        }
    }
}