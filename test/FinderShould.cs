using dependencyChange;
using FluentAssertions;
using NUnit.Framework;

namespace Tests
{
    public class FinderShould
    {

        [Test]
        public void find_users_by_querying_profile()
        {
            UserRepository repository = new UserRepository();
            Finder finder = new Finder(repository);

            finder.Find("programmer").Should().HaveCount(1);
        }

        [Test]
        public void get_users_with_empty_profile()
        {
            UserRepository repository = new UserRepository();
            Finder finder = new Finder(repository);

            finder.Find("").Should().HaveCount(1);
        }

        [Test]
        public void avoid_duplicates()
        {
            UserRepository repository = new UserRepository();
            SpecificFinder finder = new SpecificFinder(repository);

            finder.FindUnique("").Should().HaveCount(1);
        }
    }
}
