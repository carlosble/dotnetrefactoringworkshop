using NUnit.Framework;
using NUnit.Framework.Internal;
using signatureChange;

namespace Tests
{

    public class AuthenticationServiceShould
    {

        [Test]
        public void distinguish_administrator_role()
        {
            AuthenticationService service = new AuthenticationService();
            int adminId = 12345;
            Assert.True(service.IsAuthenticated(adminId));
        }


        public void distinguish_non_admin_role()
        {
            AuthenticationService service = new AuthenticationService();
            int normalUserId = 11111;
            Assert.False(service.IsAuthenticated(normalUserId));
        }
    }
}
