using constructorFusion;
using FluentAssertions;
using NUnit.Framework;

namespace Tests
{
    public class PlayerShould
    {
        [Test]
        public void increase_score_when_dragon_plays()
        {
            Player player = new Player(Role.Dragon, "WhiteDragon", 0);

            player.play();

            player.getScore().Should().Be(20);
        }

        [Test]
        public void sets_the_initial_score()
        {
            Player player = new Player(Role.Dragon, "WhiteDragon", 0);

            player.getScore().Should().Be(0);
        }
    }
}