
using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using pipelines;

namespace Tests
{
    public class AuthorShould
    {
        [Test]
        public void collect_twitter_handles()
        {
            Author author1 = new Author();
            author1.company = "Target";
            author1.twitterHandle = "@bob";
            Author author2 = new Author();
            author2.company = "Other";
            author2.twitterHandle = "@other";
            var authors = new List<Author>();
            authors.Add(author1);
            authors.Add(author2);

            Author.TwitterHandles(authors, "Target").Should().HaveCount(1);
        }
    }
}