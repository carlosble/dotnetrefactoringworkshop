using System.Collections;
using System.Collections.Generic;

namespace pipelines
{

/*
Refactoring to pipelines by Martin Fowler:

https://martinfowler.com/articles/refactoring-pipelines.html
 */

    public class Author
    {
        public string company;
        public string twitterHandle;

        static public List<string> TwitterHandles(List<Author> authors, string company)
        {
            var result = new List<string>();
            foreach (Author a in authors)
            {
                if (a.company == company)
                {
                    string handle = a.twitterHandle;
                    if (handle != null)
                        result.Add(handle);
                }
            }
            return result;
        }
    }
}