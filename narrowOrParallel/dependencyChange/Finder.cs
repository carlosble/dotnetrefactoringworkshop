using System;
using System.Collections.Generic;
using System.Linq;

namespace dependencyChange
{

    public class Finder
    {
        UserRepository repository;

        public Finder(UserRepository repository)
        {
            this.repository = repository;
        }

        public List<User> Find(string query)
        {
            return repository.FindAll()
                .Where(u => u.profile.Contains(query) || u.profile.Count == 0)
                .ToList();
        }
    }
}
