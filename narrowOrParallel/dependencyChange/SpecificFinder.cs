using System.Collections.Generic;
using dependencyChange;

namespace dependencyChange
{
    public class SpecificFinder : Finder
    {

        public SpecificFinder(UserRepository repository) : base(repository)
        {
        }

        public HashSet<User> FindUnique(string query)
        {
            return new HashSet<User>(Find(query));
        }
    }
}
