using System.Collections.Generic;
using System.Linq;

namespace dependencyChange
{

    public class Parser
    {
        public List<string> parse(string query)
        {
            return query.Split(' ').ToList();
        }
    }

}
