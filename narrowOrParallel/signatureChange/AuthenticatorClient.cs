using System;

namespace signatureChange
{

    public class AuthenticatorClient
    {
        private AuthenticationService authenticationService;

        public static void main(string[] args)
        {
            new AuthenticatorClient(new AuthenticationService()).run();
        }

        public AuthenticatorClient(AuthenticationService authenticationService)
        {
            this.authenticationService = authenticationService;
        }

        public void run()
        {
            bool authenticated = authenticationService.IsAuthenticated(33);
            Console.WriteLine("33 is authenticated = " + authenticated);
        }
    }
}
