using System;

namespace untestableLegacy { 

/*
 * This code has been extracted from OpenbravoERP:
 * https://code.openbravo.com/erp/stable/2.50-bp/file/cec9b1da72ed/src/org/openbravo/base/model/Entity.java
 */

public class Log{
	public void warn(string msg){
		Console.WriteLine(msg);
	}
}

public class Entity
    {
        private static char[] ILLEGAL_ENTITY_NAME_CHARS = new char[] {'*', '?'};
        private string name;
        public Log log = new Log();

        public void setName(string name)
        {
            // repair the name if it contains any illegal character
            this.name = removeIllegalChars(name);
        }

        private string removeIllegalChars(string fromName)
        {
            char[] chars = fromName.ToCharArray();
            var newName = "";
            bool nameChanged = false;
            foreach (char c in chars)
            {
                bool hasIllegalChar = false;
                foreach (char illegalChar in ILLEGAL_ENTITY_NAME_CHARS)
                {
                    if (c == illegalChar)
                    {
                        hasIllegalChar = true;
                        break;
                    }
                }
                if (hasIllegalChar)
                {
                    nameChanged = true;
                    continue;
                }
                newName += c;
            }
            if (nameChanged)
            {
                log.warn("The entity name " + fromName
                         + " contains illegal characters, it has been repaired to " + newName);
            }
            else
            {
                // check for other less normal characters
                foreach (char c in fromName.Trim().ToCharArray())
                {
                    bool normalChar = ('A' <= c && c <= 'Z') || ('0' <= c && c <= '9')
                                      || ('a' <= c && c <= 'z') || c == '_';
                    if (!normalChar)
                    {
                        log.warn("The entity name " + fromName + " contains a character (" + c
                                 + ") which could result in issues in HQL or "
                                 + "webservices. Use characters from a to z, A to Z or 0 to 9 or the _");
                    }
                }
            }
            return newName;
        }

    }
}
