/*
namespace untestableLegacy
{


    public class Encryptor
    {

        public string cryptWord(string word)
        {
            if (word.contains(" "))
                throw new InvalidParameterException();

            char[] wordArray = word.toCharArray();
            string newWord = "";
            for (int i = 0; i < word.length(); i++)
            {
                int charValue = wordArray[i];
                newWord += string.valueOf((char) (charValue + 2));
            }

            return newWord;
        }

        public string cryptWordToNumbers(string word)
        {
            if (word.contains(" "))
                throw new InvalidParameterException();

            char[] wordArray = word.toCharArray();
            string newWord = "";
            for (int i = 0; i < word.length(); i++)
            {
                int charValue = wordArray[i];
                newWord += string.valueOf(charValue + 2);
            }

            return newWord;
        }

        public string cryptWord(string word, string charsToReplace)
        {
            if (word.contains(" "))
                throw new InvalidParameterException();

            char[] wordArray = word.toCharArray();
            char[] replacement = charsToReplace.toCharArray();
            char[] result = wordArray.clone();
            for (int i = 0; i < wordArray.length; i++)
            {
                for (int j = 0; j < replacement.length; j++)
                {
                    if (replacement[j] == wordArray[i])
                    {
                        int charValue = wordArray[i];
                        result[i] = (char) (charValue + 2);
                    }
                }
            }
            return string.valueOf(result);
        }

        public string cryptSentence(string sentence)
        {
            char[] sentenceArray = sentence.toCharArray();
            string newWord = "";
            for (int i = 0; i < sentence.length(); i++)
            {
                int charValue = sentenceArray[i];
                newWord += string.valueOf((char) (charValue + 2));
            }

            return newWord;
        }

        public string[] getWords(string sentence)
        {
            return sentence.split(" ");
        }

        public void printWords(string sentence)
        {
            string[] words = getWords(sentence);
            for (string word :
            words)
            {
                System.out.print("<" + word + ">");
            }
        }

    }
}
*/