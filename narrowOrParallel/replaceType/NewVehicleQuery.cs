namespace replaceType
{

    public class NewVehicleQuery
    {
        public string brand;
        public string description;

        public NewVehicleQuery(string brand, string description)
        {
            this.brand = brand;
            this.description = description;
        }
    }
}
