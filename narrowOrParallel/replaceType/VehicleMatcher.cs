namespace replaceType
{

    public interface VehicleMatcher
    {
        int CountVehicles(OldVehicleQuery query);
    }
}
