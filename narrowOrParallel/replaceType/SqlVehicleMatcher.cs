namespace replaceType
{

    public class SqlVehicleMatcher : VehicleMatcher
    {

        public int CountVehicles(OldVehicleQuery query)
        {
            return new VehicleRepository().Find(new NewVehicleQuery(
                query.brandDescription.Split(' ')[0],
                query.brandDescription.Split(' ')[1]
            )).Count;
        }
    }
}
