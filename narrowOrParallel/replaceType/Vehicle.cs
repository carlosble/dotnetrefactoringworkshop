using System;

namespace replaceType
{

    public class Vehicle
    {
        public string brand;
        public string description;
        public string color;
        public DateTime soldDate;

        public Vehicle(string brand, string description)
        {
            this.brand = brand;
            this.description = description;
        }
    }
}
