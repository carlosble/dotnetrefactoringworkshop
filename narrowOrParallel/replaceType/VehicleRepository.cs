using System.Collections.Generic;

namespace replaceType
{

    public class VehicleRepository
    {
        public List<Vehicle> Find(NewVehicleQuery query)
        {
            var stub = new List<Vehicle>();
            stub.Add(new Vehicle(query.brand, query.description));
            return stub;
        }
    }
}
