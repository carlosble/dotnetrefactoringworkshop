using System;

namespace moveResponsibility
{

    public class InvoiceService
    {
        public string CalculateNetAmount(string amount, string tax)
        {
            Invoice invoice = new Invoice();
            invoice.grossAmount = Decimal.Parse(amount);
            invoice.taxPercentage = Decimal.Parse(tax);
            return (invoice.grossAmount - 
                    invoice.grossAmount * invoice.taxPercentage / 100)
                    .ToString();
                       
        }
    }
}
