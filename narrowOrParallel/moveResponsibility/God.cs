using System;

namespace moveResponsibility
{

    public class God
    {

        public int Add(int a, int b)
        {
            return a + b;
        }

        public int Substract(int a, int b)
        {
            return a - b;
        }

        public void SayHello()
        {
            Console.WriteLine("Hello!");
        }

        public void SayBye()
        {
            Console.WriteLine("Good bye!");
        }
    }

}