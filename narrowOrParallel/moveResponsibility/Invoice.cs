namespace moveResponsibility
{

    public class Invoice
    {
        public decimal grossAmount;
        public decimal taxPercentage;
        public int numberOfItems;
    }

}
