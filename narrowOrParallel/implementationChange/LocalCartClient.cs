using System;
using implementationChange;

namespace implementationChange
{
    public class LocalCartClient
    {

        public static void main(string[] args)
        {
            ShoppingCart shoppingCart = new ShoppingCart();
            shoppingCart.Add(10);
            Console.WriteLine("shoppingCart.NumberOfProducts() = " + shoppingCart.NumberOfProducts());
            Console.WriteLine("shoppingCart.CalculateTotalPrice() = " + shoppingCart.CalculateTotalPrice());
            Console.WriteLine("shoppingCart.HasDiscount() = " + shoppingCart.HasDiscount());
        }

    }
}