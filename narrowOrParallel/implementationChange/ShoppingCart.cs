namespace implementationChange
{

    public class ShoppingCart
    {
        private int price;

        public void Add(int price)
        {
            this.price = price;
        }

        public int CalculateTotalPrice()
        {
            return price;
        }

        public bool HasDiscount()
        {
            return price >= 100;
        }

        public int NumberOfProducts()
        {
            return 1;
        }
    }
}
