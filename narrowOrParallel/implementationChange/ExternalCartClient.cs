namespace implementationChange
{

/*
  Imagine this is an external client which code
  you can't change.
 */
    public class ExternalCartClient
    {
        public string formattedTotalPrice(int price)
        {
            var shoppingCart = new ShoppingCart();
            shoppingCart.Add(price);
            return string.Format("Total price is {0} euro", shoppingCart.CalculateTotalPrice());
        }
    }

}