namespace safeUnwrap
{

    public class Model
    {
        private string Color;
        private int Amount;

        public Model(string color, int amount)
        {
            Color = color;
            Amount = amount;
        }

        public string getColor()
        {
            return Color;
        }

        public int getAmount()
        {
            return Amount;
        }
    }
}