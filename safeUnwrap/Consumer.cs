namespace safeUnwrap
{

    public class Consumer
    {

        public string getColor(Model model)
        {
            Wrapper wrapper = new Wrapper(model);

            return wrapper.getWrapped().getColor();
        }
    }
}