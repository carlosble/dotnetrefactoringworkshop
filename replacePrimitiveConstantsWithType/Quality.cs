namespace replacePrimitiveConstantsWithType
{

    public class Quality
    {
        public static string Excellent = "Excellent";
        public static string Good = "Good";
        public static string Bad = "Bad";
        public static string Terrible = "Terrible";
    }
}
