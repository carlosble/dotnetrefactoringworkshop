using System;

namespace replacePrimitiveConstantsWithType
{

    public class QualityCalculator
    {
        private int score = 0;

        public void rateLocation(string quality)
        {
            if (Object.Equals(quality, Quality.Excellent))
            {
                score += 10;
            }
            if (Object.Equals(quality, Quality.Good))
            {
                score += 7;
            }
            if (Object.Equals(quality, Quality.Bad))
            {
                score -= 2;
            }
            if (Object.Equals(quality, Quality.Terrible))
            {
                score -= 10;
            }
        }

        public void rateFacilities(string quality)
        {
            if (Object.Equals(quality, Quality.Excellent))
            {
                score += 2;
            }
            if (Object.Equals(quality, Quality.Good))
            {
                score += 1;
            }
            if (Object.Equals(quality, Quality.Bad))
            {
                score -= 1;
            }
            if (Object.Equals(quality, Quality.Terrible))
            {
                score -= 2;
            }
        }

        public int getScore()
        {
            return score;
        }
    }
}